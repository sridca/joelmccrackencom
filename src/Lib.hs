{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE QuasiQuotes       #-}

module Lib where

import Prelude hiding (readFile)
import qualified Relude as R
import qualified Text.XML as X
import qualified Text.XML.Cursor as C
import qualified Data.Text as T
import qualified Data.Map as Map
import qualified Data.Maybe as M
import           Text.Hamlet.XML (xml)
import           Text.Blaze.Html.Renderer.String (renderHtml)
import           Text.Blaze.Html                 (toHtml)
-- import qualified Debug.Trace as DT
-- import Text.Blaze.Internal
-- import Control.Exception

-- data XmlMException = XmlMException T.Text
--     deriving Show

-- instance Exception XmlMException

type Alias = T.Text

type Metas = Map.Map X.Name T.Text

data XmlM
  = XmlM
    { xmlmTitle :: Maybe T.Text
    , xmlmMetas :: Metas
    , xmlmAliases :: [Alias]
    , xmlmDocument :: X.Document
    }

parseDocument :: X.Document -> XmlM
parseDocument doc =
  let
    aliases' = aliases doc
    metas' = metas doc
    title' = title doc
  in
    XmlM title' metas' aliases' doc

nodeAttributes :: X.Node -> (Maybe (Map.Map X.Name T.Text))
nodeAttributes =
  \case
    X.NodeElement e -> Just $ X.elementAttributes e
    _ -> Nothing

render :: X.Document -> String
render doc =
  let
    cursor = C.fromDocument doc
    node = C.node cursor
  in
    renderHtml $ toHtml $ document $ renderRoot node

renderRoot :: X.Node -> X.Element
renderRoot node =
  let
    foo =
      case node of
        X.NodeElement (X.Element _name _attrs children) -> children
        _ -> []
  in
    X.Element "html" Map.empty [xml|
      $forall child <- foo
        ^{goNode child}
    |]

data GlossaryTerm =
  GlossaryTerm { glossaryTerm :: String
               , glossaryDef  :: X.Node
               }

document :: X.Element -> X.Document
document root' = X.Document (X.Prologue [] Nothing []) root' []

goNode :: X.Node -> [X.Node]
goNode (X.NodeElement e) = X.NodeElement <$> goElem e
goNode (X.NodeContent t) = [X.NodeContent t]
goNode (X.NodeComment _) = [] -- hide comments
goNode (X.NodeInstruction _) = [] -- and hide processing instructions too

-- convert each source element to its XHTML equivalent
goElem :: X.Element -> [X.Element]
goElem (X.Element "todo" _attrs _children) = [] -- do not include any todos in output
goElem (X.Element "aliases" _attrs _children) = [] -- do not include any aliases in output
goElem (X.Element "para" attrs children) =
    pure $ X.Element "p" attrs $ concatMap goNode children
goElem (X.Element "em" attrs children) =
    pure $ X.Element "i" attrs $ concatMap goNode children
goElem (X.Element "strong" attrs children) =
    pure $ X.Element "b" attrs $ concatMap goNode children
goElem (X.Element "title" attrs children) =
    pure $ X.Element "h1" attrs $ concatMap goNode children
goElem (X.Element "subtitle" attrs children) =
    pure $ X.Element "h2" attrs $ concatMap goNode children
goElem (X.Element "introduction" attrs children) =
    pure $ X.Element "p" attrs $ concatMap goNode children
goElem (X.Element "term" attrs children) =
    pure $ X.Element "p" attrs $ concatMap goNode children
goElem (X.Element "name" attrs children) =
    pure $ X.Element "h3" attrs $ concatMap goNode children
goElem (X.Element "elink" attrs children) =
  let
    toAttrName = X.Name "to" Nothing Nothing
    theError = error "elink tag is missing required 'to' attribute"
    mToAttr = Map.lookup toAttrName attrs
    target = M.fromMaybe theError mToAttr
    attrs' = (Map.fromList [("href", target)])
  in
    pure $ X.Element "a" attrs' $ concatMap goNode children
goElem (X.Element "js" attrs children) =
    pure $ X.Element "pre" attrs $ concatMap goNode children

goElem (X.Element "image" attrs _children) =
    pure $ X.Element "img" (fixAttr attrs) [] -- images can't have children
  where
    fixAttr mattrs
        | "href" `Map.member` mattrs  = Map.delete "href" $ Map.insert "src" (mattrs Map.! "href") mattrs
        | otherwise                 = mattrs
goElem (X.Element name attrs children) =
    -- don't know what to do, just pass it through...
    pure $ X.Element name attrs $ concatMap goNode children

metas :: X.Document -> Map.Map X.Name T.Text
metas doc =
  let
    root' = X.documentRoot doc
  in
    X.elementAttributes root'

aliases :: X.Document -> [Alias]
aliases doc = do
  let cursor = C.fromDocument doc
  let aliases' = C.child cursor >>= C.element "aliases" >>=
                 C.child >>= C.element "alias" >>= C.child >>= C.content
  aliases'

title :: X.Document -> Maybe T.Text
title doc = do
  let cursor = C.fromDocument doc
  let aliases' = C.child cursor >>= C.element "title" >>= C.content
  R.listToMaybe aliases'
