{
# Rib library source to use
  rib ? builtins.fetchTarball "https://github.com/srid/rib/archive/bb17c2b.tar.gz"
# Cabal project root
, root ? ./.
# Cabal project name
, name ? "joelmccrackencom"
, ...
}:

import rib { inherit root name; }
